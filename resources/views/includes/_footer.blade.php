  <!-- /.footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="http://adminlte.io">aftercode.com</a>.</strong>
     All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b>5.1
    </div>
  </footer>
